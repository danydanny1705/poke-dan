module.exports = {
	'@root' 				: '.',
	'@src' 					: './src/',
	'@components' 			: './src/components/',
	'@pages' 				: './src/components/pages',
	'@util' 			: './src/util/',
};