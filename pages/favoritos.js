import React, { useEffect, useState } from 'react';
import axios from "axios";
import { Grid, Paper, Box } from "@mui/material";
import { isArray, isEmpty } from "lodash";

import Layout from "@components/Layout";
import PokemonCard from "@pages/index/PokemonCard";

const Favoritos = ({ types }) => {
  const [favoritos, setFavoritos] = useState([]);

  useEffect(() => {
    getData()
  }, [])

  const getData = () => {
    const getSavedData = JSON.parse(localStorage.getItem("favorites"));
    if (isArray(getSavedData)) {
      setFavoritos(getSavedData)
    }
  }

  return (
    <Layout>
      {isEmpty(favoritos)
        ? (
          <Box display="flex" justifyContent="center" alignItems="center" width="100%" height="100%" minHeight="100vh">
            <Paper>
              <Box p={5} fontSize={30} width={300}>
                There are no pokemons in favorites yet, go to home and add them
              </Box>
            </Paper>
          </Box>
        ) : (
          <Grid container spacing={2}>
            {favoritos.map((pokemon) => (
              <Grid key={pokemon.name} item xs={6} sm={4} md={3}>
                <PokemonCard
                  pokemon={pokemon}
                  types={types}
                  isFav={true}
                  getData={getData}
                  fav={pokemon}
                />
              </Grid>
            ))}
          </Grid>
        )}
    </Layout>
  )
}

export const getStaticProps = async () => {
  try {

    const colorTypes = [
      "gray",
      "maroon",
      "DeepSkyBlue",
      "RebeccaPurple",
      "SaddleBrown",
      "LightGray",
      "ForestGreen",
      "Indigo",
      "Silver",
      "Red",
      "RoyalBlue",
      "SeaGreen",
      "GoldenRod",
      "Black",
      "CornflowerBlue",
      "LightSalmon",
      "DarkSlateGray",
      "LightPink",
      "DarkGoldenRod",
      "DarkSlateGrey",
    ]
    const dataTypes = await axios.get("https://pokeapi.co/api/v2/type");
    const types = dataTypes.data.results.map((t, i) => ({ ...t, color: colorTypes[i] }))


    return {
      props: {
        types
      },
    }
  } catch (error) {
    return {
      props: {
        pokemons: [],
        types: []
      },
    }
  }
}

export default Favoritos

