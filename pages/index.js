import React, { useEffect, useState } from 'react';
import axios from "axios";
import { Grid, Pagination, Box, Paper } from "@mui/material"
import { trim, isArray, isEmpty, lowerCase } from "lodash";

import Layout from "@components/Layout";
import PokemonCard from "@pages/index/PokemonCard";
import Search from "@pages/index/Search";
import Notification from "@pages/index/Notification";

const Index = ({ pokemons, types, limit }) => {
  const [favoritos, setFavoritos] = useState([]);
  const [listPokemon, setListPokemon] = useState([]);
  const [pages, setPages] = useState(1);
  const [show, setShow] = useState(false);
  const [single, setSingle] = useState(false)

  const getData = () => {
    setListPokemon(pokemons.results);
    getPages()
    const getSavedData = JSON.parse(localStorage.getItem("favorites"));
    if (isArray(getSavedData)) {
      setFavoritos(getSavedData)
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const getPages = () => {
    const count = pokemons.count;
    const pag = Math.round(count / limit);
    setPages(pag)
  }

  const onChangePag = async (e, v) => {
    const { data } = await axios.get("https://pokeapi.co/api/v2/pokemon", { params: { offset: v * limit, limit } });
    setListPokemon(data.results);
  }

  const onSearch = async (e) => {
    const searchValue = lowerCase(trim(e))
    if (!!searchValue) {
      try {
        const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon/${searchValue}`);
        setListPokemon([data])
        setSingle(true)
        setShow(false)
      } catch (error) {
        getData()
        setSingle(false)
        setShow(true)
      }
    } else {
      getData()
      setSingle(false)
      setShow(false)
    }
  }

  return (
    <Layout>
      <Grid container display="flex" justifyContent="space-between" alignItems="center" >
        <Grid item xs={12} md={5}>
          <Box mb={2} py={1} px={2} component={Paper}>
            <Search setSearchResult={onSearch} />
          </Box>
        </Grid>
        <Grid item xs={12} md={5}>
          {!single &&
            <Box mb={2} py={1} px={2} component={Paper} display="flex" justifyContent={"center"}>
              <Pagination count={pages} color="primary" onChange={onChangePag} />
            </Box>
          }
        </Grid>
      </Grid>
      <Grid container spacing={2} alignContent="center">
        {listPokemon.map((pokemon) => (
          <Grid key={pokemon.name} item xs={12} sm={6} md={4} lg={3}>
            <PokemonCard
              pokemon={pokemon}
              types={types}
              isFav={!isEmpty(favoritos.find((f) => f.name === pokemon.name))}
              fav={favoritos.find((f) => f.name === pokemon.name)}
            />
          </Grid>
        ))}
      </Grid>
      <Notification show={show} onClose={() => setShow(false)} />
    </Layout>
  )
}

export const getStaticProps = async () => {
  try {
    const limit = 8;
    const { data } = await axios.get("https://pokeapi.co/api/v2/pokemon", { params: { offset: 0, limit } });

    const colorTypes = [
      "gray",
      "maroon",
      "DeepSkyBlue",
      "RebeccaPurple",
      "SaddleBrown",
      "LightGray",
      "ForestGreen",
      "Indigo",
      "Silver",
      "Red",
      "RoyalBlue",
      "SeaGreen",
      "GoldenRod",
      "Black",
      "CornflowerBlue",
      "LightSalmon",
      "DarkSlateGray",
      "LightPink",
      "DarkGoldenRod",
      "DarkSlateGrey",
    ]
    const dataTypes = await axios.get("https://pokeapi.co/api/v2/type");
    const types = dataTypes.data.results.map((t, i) => ({ ...t, color: colorTypes[i] }))


    return {
      props: {
        pokemons: data,
        limit,
        types
      },
    }
  } catch (error) {
    return {
      props: {
        pokemons: [],
        types: []
      },
    }
  }
}

export default Index

