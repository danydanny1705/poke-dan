import '../styles/globals.css'
import { createTheme, ThemeProvider } from '@mui/material/styles';

import ClientOnly from "@util/ClientOnly";

function MyApp({ Component, pageProps }) {

  const theme = createTheme();


  return (
    <ClientOnly>
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </ClientOnly>
  )
}

export default MyApp
