import React from 'react';
import { Paper, Box } from "@mui/material";

import Layout from "@components/Layout";

const Error404 = () => {
  return (
    <Layout>
      <Box display="flex" justifyContent="center" alignItems="center" width="100%" height="100%" minHeight="100vh">
        <Paper>
          <Box p={5} fontSize={30} width={300} textAlign="center">
            404 Page no Found
          </Box>
        </Paper>
      </Box>
    </Layout>
  )
}

export const getStaticProps = async () => {
  try {

    return {
      props: {
      },
    }
  } catch (error) {
    return {
      props: {
      },
    }
  }
}

export default Error404

