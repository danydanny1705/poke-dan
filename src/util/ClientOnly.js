import React, { useState, useEffect, Fragment } from "react";

const ClientOnly = ({ children, ...delegated }) => {
    /*
        Solventa el errror: Prop className did not match In Material UI
    */
    const [hasMounted, setHasMounted] = useState(false);

    useEffect(() => {
        setHasMounted(true);
    }, []);

    if (!hasMounted) return null

    return (
        <Fragment {...delegated}>
            {children}
        </Fragment>
    );
}

export default ClientOnly