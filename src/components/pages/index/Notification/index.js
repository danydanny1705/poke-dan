import React, { useState, useEffect } from 'react';
import {
    Snackbar,
    Alert,
    IconButton,
} from "@mui/material"
import CloseIcon from '@mui/icons-material/Close';

const Notification = ({ show, onClose }) => {
    const [open, setOpen] = useState(false);

    useEffect(() => {
        setOpen(show)
    }, [show])

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        onClose(false);
    };

    const action = (
        <React.Fragment>
            <IconButton
                size="small"
                aria-label="close"
                color="inherit"
                onClick={handleClose}
            >
                <CloseIcon fontSize="small" />
            </IconButton>
        </React.Fragment>
    );

    return (
        <div>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    Pokemon no found
                </Alert>
            </Snackbar>
        </div>
    );
}

export default Notification;