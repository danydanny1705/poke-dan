import React, { useState } from 'react';
import {
    Box,
    InputBase,
    IconButton,
    Divider
} from "@mui/material"
import SearchIcon from '@mui/icons-material/Search';
import CloseIcon from '@mui/icons-material/Close';

const SearchPokemon = ({ setSearchResult, }) => {
    const [value, setValue] = useState("")

    const onChange = (e) => {
        setValue(e.target.value)
    }
    const onSearch = () => {
        setSearchResult(value)
    }
    const onClear = () => {
        setValue("")
        setSearchResult("")
    }

    return (
        <Box display="flex" height={30}>
            <InputBase
                sx={{ ml: 1, flex: 1, width: 200 }}
                placeholder="Search Pokemon Name"
                inputProps={{ 'aria-label': 'search pokemon name' }}
                onChange={onChange}
                value={value}
            />
            <IconButton type="submit" sx={{ p: '0px' }} aria-label="search" onClick={onSearch}>
                <SearchIcon />
            </IconButton>
            <Divider sx={{ height: 28, mx: 2 }} orientation="vertical" />
            <IconButton type="submit" sx={{ p: '0px' }} aria-label="search" onClick={onClear}>
                <CloseIcon />
            </IconButton>
        </Box>
    );
}

export default SearchPokemon;
