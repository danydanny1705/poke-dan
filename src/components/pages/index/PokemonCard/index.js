import React, { useEffect, useState } from 'react';
import axios from "axios";
import { capitalize, isArray, isEmpty } from "lodash";
import {
  Card,
  CardActionArea,
  CardMedia,
  Box,
  Chip,
  Stack,
  IconButton,
  Paper,
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import FavoriteTwoToneIcon from '@mui/icons-material/FavoriteTwoTone';
import moment from "moment"

import { useStyle } from "./Styled"

const PokemonCard = ({ pokemon, types, isFav, getData = () => { }, fav = {} }) => {
  const [state, setState] = useState({ loading: false, data: {}, error: false })
  const classes = useStyle({ ...state });
  const [isFavorite, setIsFavorite] = useState(false)
  const [showDetail, setShowDetail] = useState(false)
  const [open, setOpen] = React.useState(false);
  const [nickName, setNickName] = useState("")
  const [date, setDate] = useState("")
  const url = "https://pokeapi.co/api/v2/pokemon"

  useEffect(() => {
    getPokemonDetail()
  }, []);

  useEffect(() => {
    setIsFavorite(isFav)
  }, [isFav])

  const askAddFavorites = () => {
    setOpen(true);
  }

  const addFavorites = (action = "update") => {
    const getSavedData = JSON.parse(localStorage.getItem("favorites"));
    if (isArray(getSavedData)) {
      if (!isEmpty(getSavedData.find((f) => f.name === pokemon.name))) {
        const getPosition = ((e) => e.name === pokemon.name);
        const position = getSavedData.findIndex(getPosition)

        if (action === "delete") {
          getSavedData.splice(position, 1)
          setIsFavorite(false)
          savedFav(getSavedData)
        } else {
          getSavedData.splice(position, 1, {
            name: pokemon.name,
            nickName: nickName,
            date,
          })
          setIsFavorite(true)
          savedFav(getSavedData)
        }
        getData()
      } else {
        getSavedData.push({
          name: pokemon.name,
          nickName: nickName || pokemon.name,
          date,
        })
        savedFav(getSavedData)
        setIsFavorite(true)
      }
    } else {
      const newArr = [{
        name: pokemon.name,
        nickName: nickName || pokemon.name,
        date,
      }]
      savedFav(newArr)
      setIsFavorite(true)
    }
    handleClose({ reset: false })
  }

  const savedFav = (fav) => {
    localStorage.setItem("favorites", JSON.stringify(fav))
  }

  const getPokemonDetail = async () => {
    setState({ loading: true, data: {}, error: false })
    try {
      const resp = await axios.get(`${url}/${pokemon.name}`)
      const data = resp.data;
      const species = await axios.get(data.species?.url)
      data.species = species.data;
      setState({ loading: false, data, error: false })
      setNickName(fav.nickName || data.name);
      setDate(fav.date || Date())
    } catch (error) {
      console.log(error);
      setState({ loading: false, data: {}, error: true })
    }
  }

  const onDetails = () => {
    setShowDetail(!showDetail)
  }

  const handleClose = ({ reset = true }) => {
    setOpen(false);
    if (reset) {
      setNickName(fav.nickName);
    }
  };

  return (
    <Card className={classes.root} elevation={5}>
      <Box position="absolute" right={0} zIndex={2}>
        <IconButton aria-label="delete" size="large" onClick={askAddFavorites}>
          {isFavorite
            ? (
              <Box color="#FF0000" border="1px">
                <FavoriteTwoToneIcon color="inherit" />
              </Box>
            ) : (
              <FavoriteBorderIcon />
            )
          }
        </IconButton>
      </Box>
      <CardActionArea onClick={onDetails}>
        {showDetail
          ? (
            <>
              <Box>
                <Box className={classes.titleDetail}>
                  {capitalize(state.data.name)}
                </Box>
              </Box>
              <Box className={classes.detail}>
                {isFavorite &&
                  <>
                    <Box fontWeight="bold" fontSize={17}>Personals:</Box>
                    <Box>
                      <Box component="span" >Nick:</Box>
                      <Box component="span" className={classes.text}>{capitalize(nickName)}</Box>
                    </Box>
                    <Box>
                      <Box component="span" >Update:</Box>
                      <Box component="span" className={classes.text}>{moment(date).format("LLL")}</Box>
                    </Box>
                    <br />
                  </>
                }
                <Box fontWeight="bold" fontSize={17}>Generals:</Box>
                <Box>
                  <Box component="span" >Habitat:</Box>
                  <Box component="span" className={classes.text}>{capitalize(state.data.species?.habitat?.name)}</Box>
                </Box>
                <Box>
                  <Box component="span" >Shape:</Box>
                  <Box component="span" className={classes.text}>{capitalize(state.data.species?.shape?.name)}</Box>
                </Box>
                <Box>
                  <Box component="span" >Varieties:</Box>
                  <Box component="span" className={classes.text}>{capitalize(state.data.species?.varieties?.map((v) => v.pokemon.name)?.join(", "))}</Box>
                </Box>
                <Box>
                  <Box component="span" >Weight:</Box>
                  <Box component="span" className={classes.text}>{`${state.data.weight}kg`}</Box>
                </Box>
                <br />
                <Box fontWeight="bold" fontSize={17}>Stats:</Box>
                <Box>
                  {state.data.stats?.map((stat, i) => (
                    <Box key={i}>
                      <Box component="span" >{`${capitalize(stat.stat.name)}:`}</Box>
                      <Box component="span" className={classes.text}>{stat.base_stat}</Box>
                    </Box>
                  ))}
                </Box>
                <br />
              </Box>
            </>
          ) : (
            <>
              <Box className={classes.imgContainer}>
                <CardMedia
                  classes={{ root: classes.img }}
                  component="img"
                  alt={state.data.name}
                  image={state.data.sprites?.other?.dream_world?.front_default}
                />
              </Box>
              <Box className={classes.content}>
                <Box className={classes.title}>
                  {capitalize(state.data.name)}
                  <Box fontSize={12}>
                    {isFavorite && `(${capitalize(nickName)})`}
                  </Box>
                </Box>
                <Stack direction="row" spacing={1} justifyContent="center">
                  {state.data.types?.map((type) => (
                    <Chip
                      key={type.type.name}
                      size="small"
                      label={
                        <Box color="#fff" >
                          {capitalize(type.type.name)}
                        </Box>
                      }
                      color="default"
                      style={{ backgroundColor: types.find((t) => t.name === type.type.name)?.color }}
                    />
                  ))}
                </Stack>
              </Box>
              <Box display="flex" justifyContent="center" my={2}>
                <Paper className={classes.btn} variant="contained" size="small" elevation={5} >
                  Detail
                </Paper>
              </Box>
            </>
          )
        }
      </CardActionArea>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Favorites</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Give your pokemon a nick name to save it
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Nick Name"
            type="text"
            fullWidth
            variant="standard"
            onChange={(e) => setNickName(e.target.value)}
            value={nickName}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">Cancel</Button>
          {isFavorite && <Button onClick={() => addFavorites("delete")} color="secondary">Delete</Button>}
          <Button onClick={addFavorites} >Accept</Button>
        </DialogActions>
      </Dialog>
    </Card>
  );
}

export default PokemonCard;
