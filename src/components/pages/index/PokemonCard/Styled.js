import { makeStyles } from '@mui/styles';

export const useStyle = makeStyles((theme) => ({
    root: {
        margin: "auto",
        position: "relative",
        maxWidth: 300,
        minHeight: 385,
        borderRadius: 10,
        background: "rgb(255,255,255)",
        background: ({ data }) => `linear-gradient(135deg, rgba(255,255,255,1) 0%, ${data?.species?.color?.name} 100%)`,
        transition: "all 0.2s"
    },
    title: {
        fontSize: 25,
        textAlign: "center",
        fontWeight: 700,
        margin: theme.spacing(0, 0, 2),
        color: "#FFF",
    },
    titleDetail: {
        fontSize: 25,
        textAlign: "center",
        fontWeight: 700,
        color: "#000",
        padding: theme.spacing(1, 5, 0.5, 1),
        boxShadow: "5px 5px 5px -3px rgba(0,0,0,0.5)"
    },
    content: {
        margin: theme.spacing(3, 2, 2),
        padding: theme.spacing(2),
        backgroundColor: "rgba(0,0,0,0.6)",
        borderRadius: "5px"
    },
    imgContainer: {
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-end",
        height: 200,
    },
    img: {
        backgroundColor: "#FFF",
        borderRadius: "50%",
        height: 150,
        width: 150,
        maxWidth: 150,
        objectPosition: "center center",
        objectFit: "contain !important",
        filter: `drop-shadow(10px 10px 10px rgba(0,0,0,0.6))`,
        boxShadow: "inset -5px -5px 20px -5px"
    },
    btn: {
        border: "1px solid rgba(0,0,0,0.1)",
        background: "#FFF",
        color: "#000",
        padding: theme.spacing(0.5, 2),
        boxShadow: "5px 5px 5px -3px rgba(0,0,0,0.5)"
    },
    detail: {
        margin: theme.spacing(1, 2, 2),
        padding: theme.spacing(2),
        backgroundColor: "rgba(0,0,0,0.6)",
        borderRadius: "5px",
        minHeight: 320,
        fontSize: 16,
        color: "#FFF",
    },
    text: {
        padding: theme.spacing(0, 2),
        color: "silver"
    }
}));
