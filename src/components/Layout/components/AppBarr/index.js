import * as React from 'react';
import {
    AppBar,
    Box,
    Toolbar,
    Button,
} from '@mui/material';
import Link from 'next/link'


const ButtonAppBar = () => {
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar style={{ minHeight: 50 }}>
                    <Box sx={{ flexGrow: 1 }} >
                        <Link href="https://gitlab.com/danydanny1705/poke-dan">
                            <Button color="inherit">
                                Poke Repo
                            </Button>
                        </Link>
                    </Box>
                    <Link href="/">
                        <Button color="inherit">
                            <Box>Home</Box>
                        </Button>
                    </Link>
                    <Link href="/favoritos">
                        <Button color="inherit">
                            <Box>Favorites</Box>
                        </Button>
                    </Link>
                </Toolbar>
            </AppBar>
        </Box>
    );
}
export default ButtonAppBar
