import { makeStyles } from '@mui/styles';

export const useStyle = makeStyles((theme) => ({
    root: {
        background: " #3a6186",
        background: "-webkit-linear-gradient(to right, #89253e, #3a6186)",
        background: "linear-gradient(to right, #89253e, #3a6186)",
        minHeight: "100vh"
    },
    content: {
        maxWidth: 1200,
        margin: 'auto',
        padding: theme.spacing(2, 1)
    },
}));
