import React from 'react';
import {
    Box
} from "@mui/material";
import Head from 'next/head'

import { useStyle } from "./Styled";
import AppBarr from "./components/AppBarr";

const Layout = ({ children }) => {
    const classes = useStyle()

    return (
        <Box className={classes.root}>
            <Head>
                <title>Poke Dan</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <AppBarr />
            <Box className={classes.content}>
                {children}
            </Box>
        </Box>
    );
};

export default Layout;
